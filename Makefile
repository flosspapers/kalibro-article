LATEX = pdflatex
TARGET = dissertacao

all: $(TARGET).tex 
	$(LATEX) $<
	bibtex $(TARGET) 
	makeindex $(TARGET) 
	$(LATEX) $<
	$(LATEX) $<
	$(LATEX) $<
	evince $(TARGET).pdf &

edit:
	gedit Makefile *.bib *.tex &

clean:
	rm -f *.aux *.bbl *.blg *.brf *.idx *.ilg *.ind *.lof *.log *.lot *.out *.toc *~
