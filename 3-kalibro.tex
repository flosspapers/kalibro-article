\chapter{Kalibro Metrics}
\label{chap:kalibro}

Desenvolvemos a Kalibro na linguagem Java, de forma modular e desacoplada, para facilitar modificações, incrementos e
incorporações de outras ferramentas e diferentes interfaces de usuário.
%
Isolamos toda lógica, processamento e persistência em um núcleo independente de interface com o usuário que chamamos
de KalibroCore.
%
Ele inclui uma fachada \citep{DesignPatterns} para acessar toda sua funcionalidade, a classe \verb|KalibroFacade|.
%
Assim, as únicas classes que são usadas fora do KalibroCore são \verb|KalibroFacade| e suas dependências de interface.
%
Desenvolvemos uma interface Web Service - o Kalibro Service - para acessar as funcionalidades da Kalibro remotamente.
%
A Figura \ref{fig:interacoes} mostra as duas implementações da fachada e como elas fornecem as funcionalidades da
Kalibro.

\begin{figure}[!h]
  \centering
  \includegraphics[width=0.25\textwidth]{fachada}
  \caption{Fachada pode ser local ou remota}
  \label{fig:fachada}
\end{figure}

%%
Para interagir com a Kalibro, desenvolvemos uma interface Swing, a Kalibro Desktop.
%
A Kalibro Desktop se comunica com a fachada do KalibroCore, assim o usuário pode configurá-la como aplicativo
independente ou como cliente de um Kalibro Service remoto, variando apenas a implementação da \verb|KalibroFacade|.
%
Quando usado como cliente, a instalação completa (banco de dados, coletor, etc) só precisa ser feita no servidor e
vários terminais podem utilizá-la com pouca configuração.

%%
As sessões seguintes deste capítulo correspondem aos requisitos estabelecidos e como a Kalibro foi desenhada para
atendê-los.
%
Quando tratarmos de interação com o usuário, mostraremos capturas de tela da Kalibro Desktop.
%
Entretanto, não pretendemos defender a forma como as métricas são visualizadas nesse aplicativo.
%
Visualização de métricas é em si um campo de pesquisa.
%
Concentramos nossos esforços em desenvolver um serviço de avaliação qualitativa de métricas de código-fonte, não o
\textit{front-end}.

%% ------------------------------------------------------------------------------------------------------------------ %%
\section{Configuração de intervalos}
\label{sec:intervalos}

Adotamos o conceito de configuração de métricas.
%
Uma configuração é um conjunto de métricas, cada métrica contendo um conjunto de intervalos.
%
Essa configuração é criada e personalizada pelo usuário.
%
Cada intervalo possui, além de início e fim, nota, cor para exibição e comentários pertinentes para quando o valor da
métrica estiver dentro dele (veja a Figura \ref{fig:intervalo}).
%
Dessa forma, é possível associar uma avaliação qualitativa ao valor obtido como resultado do cálculo da métrica.

\begin{figure}[!h]
  \centering
  \includegraphics[width=0.5\textwidth]{intervalo}
  \caption{Editando um intervalo}
  \label{fig:intervalo}
\end{figure}

Quando um projeto de software é analisado, apenas os resultados das métricas contidas na configuração associada são
exibidos, com seus valores associados aos intervalos correspondentes.
%
A figura \ref{fig:resultados} mostra resultados associados à avaliação qualitativa (configuração de intervalos).
%
O usuário pode criar várias configurações e utilizá-las para atender demandas específicas de cada projeto.

\begin{figure}[!h]
  \centering
  \includegraphics[width=1.0\textwidth]{resultados}
  \caption{Resultados avaliados por uma configuração}
  \label{fig:resultados}
\end{figure}


\section{Extensível}
\label{sex:extensivel}

A Kalibro Metrics foi projetada para ser facilmente acoplada a diferentes ferramentas de coleta de métricas de
código-fonte.
%
A interface \verb|MetricCollector| é como a Kalibro interage com as ferramentas coletoras de métricas.
%
Utilizamos Analizo como coletor padrão, desenvolvendo uma implementação dessa interface.
%
É ela que deve ser implementada para cada ferramenta com a qual a Kalibro se conecte.
%
Ela é constituída de apenas 3 métodos:

\begin{itemize}
	\item \verb|getSupportedMetrics()| especifica quais métricas a ferramenta suporta.
	\item \verb|getSupportedLanguages()| especifica quais linguagens de programação a ferramenta pode analisar.
	\item \verb|collectMetrics(codeAddress, language)| coleta os resultados das métricas.
\end{itemize}

\begin{figure}[!h]
  \centering
  \includegraphics[width=1.0\textwidth]{MetricCollector}
  \caption{Interface para coleta de métricas que proporciona extensibilidade}
  \label{fig:coletor}
\end{figure}

Desse modo, o usuário pode escolher o coletor de acordo com as métricas que ele oferece e a linguagem de seu projeto.
%
A Figura \ref{fig:coletor} mostra como essa arquitetura deixa a Kalibro extensível para incorporação de novas métricas e
linguagens de programação.
%
A Figura \ref{fig:interacoes} apresenta como a Kalibro interage com diferentes serviços, interfaces de usuário e
coletores de métricas.

\begin{figure}[!h]
  \centering
  \includegraphics[width=0.5\textwidth]{interacoes}
  \caption{Diagrama de interações da Kalibro}
  \label{fig:interacoes}
\end{figure}

O outro quesito da extensibilidade é a capacidade de criar novas métricas a partir das existentes.
%
Dividimos então as métricas utilizadas na Kalibro em métricas nativas e compostas.
%
As métricas nativas são as fornecidas pelo coletor e as compostas são criadas a partir das nativas através de scripts.
%
Após coletar os resultados das métricas nativas, a Kalibro calcula estatísticas e valores das métricas compostas.
%
A Figura \ref{fig:metrica_composta} mostra a criação, pelo usuário, da métrica SC (complexidade estrutural) a partir das
métricas LCOM4 (coesão) e CBO (acoplamento).

\begin{figure}[!h]
  \centering
  \includegraphics[width=0.7\textwidth]{metrica_composta}
  \caption{Criação de uma métrica composta}
  \label{fig:metrica_composta}
\end{figure}

Resolvemos integrar à Kalibro o download automático de código-fonte a partir de seu repositório por diversas razões.
%
Por amigabilidade, pois o usuário precisa apenas indicar o endereço do código ao invés de baixá-lo e depois executar a
Kalibro.
%
Para dar suporte è utilização da Kalibro como serviço Web, pois enviar um endereço para o serviço remoto é muito mais
conveniente que anexar o código-fonte.
%
Para dar suporte a pesquisas que envolvem vários projetos, facilitando o download para compará-los.
%
Finalmente, para facilitar o acompanhamento da evolução dos resultados das métricas de um projeto ao longo do tempo.

%%
A Kalibro carrega código-fonte de um repositório executando chamadas de sistema.
%
Para adicionar um novo tipo de repositório basta implementar a interface \verb|ProjectLoader| e resgistrar o novo tipo
de repositório.
%
O principal método dessa interface é o \verb|getLoadCommands|, que recebe o endereço de um repositório e devolve os
comandos a serem executados para baixar o código.
%
Dessa forma, a Kalibro também é extensível para diversos tipos de repositório.
%
Já implementamos o suporte a Subversion, Mercurial, Git, Tarball e Zip.
%
A figura \ref{fig:repositorio} mostra como essa arquitetura facilita a inclusão de novos tipos de repositório.

\begin{figure}[!h]
  \centering
  \includegraphics[width=0.7\textwidth]{repositorio}
  \caption{Selecionando o tipo de respositório de um projeto}
  \label{fig:repositorio}
\end{figure}

\section{Comparação entre projetos}
\label{sec:comparacao}

Além do conjunto de intervalos, cada métrica de uma configuração possui um peso.
%
Com a nota dos intervalos e o peso de cada métrica, a Kalibro calcula uma nota geral do projeto, pacote, classe ou
método, com uma média ponderada.
%
Desse modo, além de ser uma forma de interpretar resultados de métricas, as configurações também servem como critério
específico de avaliação e comparação entre projetos de software.
%
Na Figura \ref{fig:resultados} mostramos a tela de resultados, na qual a nota do projeto aparece no canto superior
direito.

As configurações de métricas são a parte mais importante da flexibilidade da Kalibro.
%
A configuração utilizada por um gerente de projeto para acompanhar a qualidade do código pode sofrer melhorias ao longo
do tempo.
%
No contexto de outros trabalhos, nosso grupo de pesquisa está desenvolvendo uma rede social
\footnote{Mezuro: http://softwarelivre.org/mezuro/} que servirá de plataforma para o compartilhamento dessas
configurações, facilitando uma discussão em torno delas e sua subsequente evolução.


\section{Software Livre}

Kalibro Metrics é software livre, distribuído sob a licença GPL (GNU Lesser General Public License) versão 3.
%
Seu código-fonte, assim como pacotes binários, manuais e tutoriais podem ser encontrados em  \verb|http://kalibro.org|.

Desenvolvemos sua arquitetura pensando em modularidade e flexibilidade, facilitando o recebimento de contribuições da
comunidade de software livre.
%
A seguir, outras características que torna seu código receptível a contribuições.

O download e a análise de projetos é executada de forma assíncrona.
%
Ouvintes podem ser registrados para receber notificações quando o projeto passa por cada uma dessas fases.
%
Ao usar a Kalibro como cliente do Kalibro Service, um adaptador especial faz \textit{polling} do estado do(s) projeto(s)
e notifica os ouvintes, encapsulando detalhes de conexão com o serviço.

%%
Todo tratamento de threads é encapsulado em um só pacote.
%
É possível criar tarefas e executá-las assincronamente, adiocionando um ouvinte à tarefa.
%
O ouvinte recebe um relatório que informa se a tarefa completou normalmente, mostrando a exceção capturada se for o
caso.
%
Classes utilitárias contêm métodos para executar tarefas de forma síncrona, assíncrona, periódica e com
\textit{timeouts}.
%
Também existe uma interface simples para executar tarefas na linha de comando e salvar ou capturar sua saída.

%%
A Kalibro usa JPA - Java Persistence API \citep{JPA} - para persistir suas entidades.
%
Com pouca configuração, ela pode ser instalada para usar qualquer banco de dados com suporte a JPA.

\section{Caso de uso}
\label{sec:casos_de_uso}

Kalibro Metrics teve sua primeira versão estável lançada em dezembro de 2010 e já tem sido usada por outros grupos de
pesquisa.
%
Kalibro Metrics foi integrada à QualiPSo Quality Platform através da Spago4Q \footnote{http://spago4q.org}.
%
O projeto QualiPSo começou em 2007, limitado a análise de projetos Java.
%
Em 2010, para analisar C e C++, nós integramos a Kalibro Metrics via Kalibro Service dentro do projeto QualiPSo.

\begin{figure}[!h]
  \centering
	\includegraphics[width=0.37\textwidth]{qualipso}
  \caption{Integração com a Kalibro no QualiPSo}
  \label{fig:qualipso}
\end{figure}

A Figura \ref{fig:qualipso} apresenta como a Spago4Q se comunica com o Kalibro Service.
%
A Spago4Q coleta resultados a partir de extratores.
%
No nosso caso, os extratores usam a classe de fachada do Kalibro Service, simplificando a interação.
%
O extrator de upload lê requisição da fila da Spago4Q e envia requisições para a Kalibro processá-las.
%
O extrator de análise consulta o serviço e fornece os valores de volta para a Spago4Q.
