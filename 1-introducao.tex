\chapter{Introdução}
\label{cap:introducao}

Qualquer que seja a metodologia de desenvolvimento, monitorar a qualidade do software é fundamental.
%
Dentre as inúmeras características que fazem um bom software, várias delas podem ser percebidas no código-fonte, e
algumas são exclusivas dele.
%
Por exemplo, código compilado pode ser analisado, mas características como organização e legibilidade são perdidas;
mesmo uma bateria de testes com ótima cobertura só apresenta informação sobre o funcionamento atual, não refletindo
manutenibilidade, modularidade, flexibilidade e simplicidade.
%
Nesse contexto, as métricas de código-fonte completam as outras abordagem de monitoramento da qualidade do software.

%%
Engenheiros de software e pesquisadores precisam analisar códigos-fonte para entender melhor projetos de software.
%
Com o sucesso dos Métodos Ágeis \citep{Beck1999} e do Software Livre \citep{FSF}, o código-fonte é destacado como tendo
papel central nas atividades de desenvolvimento de software.
%
As funcionalidades são entregues constantemente aos clientes e usuários, portanto o código-fonte é escrito gradualmente
e diferentes desenvolvedores fazem atualizações e melhorias continuamente \citep{Martin2008}.
%
Assim, novas funcionalidades são inseridas e erros corrigidos durante as iterações de desenvolvimento e manutenção.

%%
Desenvolvedores tomam decisões sobre o código enquanto estão programando.
%
Em conjunto, essas decisões influenciam na qualidade do código \citep{Beck2007}.
%
Observar os atributos do código que está desenvolvendo pode auxiliar na tomada dessas decisões.
%
No contexto dos projetos de software livre, há estudos que mostram que a motivação para se envolver com um projeto de
software livre é criticamente afetada pela modularidade do seu código \citep{BaldwinClark2006} e sua organização e
complexidade influenciam o número de downloads e membros \citep{Meirelles2010}.

%%
De modo geral, existe uma diferença significativa, tanto em tempo gasto como em quantidade de linhas que um
desenvolvedor lê e escreve.
%
Como é preciso entender uma implementação lendo código (inclusive escrito no passado pelo próprio leitor) para poder
melhorará-la ou incrementá-la, código-fonte deve ser escrito para ser entendido por pessoas, mais do que pela máquina
\citep{Martin2008}.
%
Nesse cenário, métricas de código-fonte podem auxiliar no desenvolvimento de código claro, simples e flexível.
%
A partir de uma coleção de métricas coletadas automaticamente e de uma forma objetiva de interpretar seus valores,
engenheiros de software podem monitorar características específicas do seu código - assim como implementações
problemáticas - para tomar melhores decisões ao (re)escrevê-las.

%% ------------------------------------------------------------------------------------------------------------------ %%
\section{Problemática}
\label{sec:problematica}

%%
Métricas de código-fonte foram propostas desde quando os primeiros conceitos da engenharia de software surgiram.
%
As métricas pioneiras foram rapidamente absorvidas pela indústria, que as usa massivamente.
%
Métricas de complexidade e tamanho são as mais usuais, por exemplo LOC (linhas de código) e seus derivados (erros/LOC,
LOC/tempo).
%
Essas métricas são reconhecidamente limitadas (e até perigosas) quando usadas isoladamente.
%
Mas mesmo com o avanço da pesquisa em métricas desde então, a indústria continua usando as mesmas métricas simplistas e
de forma isolada \citep{Fenton1999}.

Especialistas em métricas podem ser capazes de entender e usar métricas, mas todos desenvolvedores deveriam saber como
usá-las para monitorar e melhorar seu código.
%
Para facilitar isso, é necessária uma ferramenta que sistematize a avaliação do código-fonte.
%
As ferramentas de coleta de métricas de código-fonte possuem, em geral, uma ou mais dessas carências:
\begin{itemize}
  \item Associação entre resultados numéricos e forma de interpretá-los. Ferramentas de métricas frequentemente mostram
    seus resultados como valores numéricos isolados para cada métrica.
  \item Flexibilidade nessa interpretação. Algumas ferramentas mostram indicadores booleanos (bom ou ruim) para os
    valores das métricas, mas não há forma de configurar os valores de referências.
  \item Flexibilidade para funcionar com diferentes linguagens. As ferramentas que não pecam nos quesitos anteriores
    foram desenhadas especificamente para uma só linguagem de programação.
  \item Capacidade de estender seu uso com novas métricas, além das embutidas na ferramenta.
\end{itemize}

%% ------------------------------------------------------------------------------------------------------------------ %%
\section{Contribuições}
\label{sec:contribuicoes}

Neste trabalho apresentamos a Kalibro Metrics, uma ferramenta de software livre, projetada para ser conectada em
qualquer ferramenta de coleta de métricas de código-fonte, a estendendo para fornecer avaliação de fácil entendimento
para a qualidade do software analisado.

Kalibro Metrics permite que um especialista em métricas (ou qualquer usuário) especifique conjuntos de intervalos de
aceitação para cada métrica fornecida pela ferramenta base, além de possibilitar a criação de novas métricas, combinando
as básicas.
%
Com isso pretendemos difundir o uso de métricas de código-fonte, pois com um conjunto de intervalos e suas respectivas
interpretações, qualquer desenvolvedor pode explorá-las e melhor entendê-las.

%% ------------------------------------------------------------------------------------------------------------------ %%
\section{Organização do Trabalho}
\label{sec:organizacao_do_trabalho}

O Capítulo \ref{chap:requisitos} descreve os requisitos que acreditamos ser essenciais em uma ferramenta que vise a
difundir o uso de métricas de código-fonte.
%
O não-preenchimento desses requisitos nas ferramentas estudadas irá justificar nossa escolha de criar a Kalibro Metrics.
%
O Capítulo \ref{chap:kalibro} descreve a Kalibro Metrics, como sua arquitetura foi desenhada para facilitar o
preenchimento desses requisitos e apresenta um caso de uso da Kalibro.
%
Finalmente, o Capítulo \ref{chap:conclusao} mostra o cronograma para conclusão deste trabalho.
